package it.uniroma2.art.converter.vocabolary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

public class Lemon {

    public static final String URI = "http://lemon-model.net/lemon";
    public static final String NAMESPACE = "http://lemon-model.net/lemon#";

    public static final String LEXICAL_ENTRY = "http://lemon-model.net/lemon#LexicalEntry";
    public static final String LEXICAL_SENSE = "http://lemon-model.net/lemon#LexicalSense";
    public static final String FORM = "http://lemon-model.net/lemon#Form";
    public static final String PART = "http://lemon-model.net/lemon#Part";
    public static final String PHRASE = "http://lemon-model.net/lemon#Phrase";
    public static final String WORD = "http://lemon-model.net/lemon#Word";
    public static final String LEXICON = "http://lemon-model.net/lemon#Lexicon";

    public final static String REPRESENTATION = "http://lemon-model.net/lemon#representation";
    public final static String WRITTEN_REP = "http://lemon-model.net/lemon#writtenRep";

    public final static String LEXICAL_FORM = "http://lemon-model.net/lemon#lexicalForm";
    public final static String CANONICAL_FORM = "http://lemon-model.net/lemon#canonicalForm";
    public final static String OTHER_FORM = "http://lemon-model.net/lemon#otherForm";
    public final static String ABSTRACT_FORM = "http://lemon-model.net/lemon#abstractForm";

    public final static String SENSE = "http://lemon-model.net/lemon#sense";
    public final static String IS_SENSE_OF = "http://lemon-model.net/lemon#isSenseOf";

    public final static String REFERENCE = "http://lemon-model.net/lemon#reference";
    public final static String ALT_REF = "http://lemon-model.net/lemon#altRef";
    public final static String PREF_REF = "http://lemon-model.net/lemon#prefRef";
    public final static String HIDDEN_REF = "http://lemon-model.net/lemon#hiddenRef";
    public final static String IS_REFERENCE_OF = "http://lemon-model.net/lemon#isReferenceOf";

    public final static String LANGUAGE = "http://lemon-model.net/lemon#language";
    public final static String ENTRY = "http://lemon-model.net/lemon#entry";

    public static class Res {

        public static ARTURIResource URI;
        public static ARTURIResource NAMESPACE;

        public static ARTURIResource LEXICAL_ENTRY;
        public static ARTURIResource LEXICAL_SENSE;
        public static ARTURIResource FORM;
        public static ARTURIResource PART;
        public static ARTURIResource PHRASE;
        public static ARTURIResource WORD;
        public static ARTURIResource LEXICON;

        public static ARTURIResource REPRESENTATION;
        public static ARTURIResource WRITTEN_REP;

        public static ARTURIResource LEXICAL_FORM;
        public static ARTURIResource CANONICAL_FORM;
        public static ARTURIResource OTHER_FORM;
        public static ARTURIResource ABSTRACT_FORM;

        public static ARTURIResource SENSE;
        public static ARTURIResource IS_SENSE_OF;

        public static ARTURIResource REFERENCE;
        public static ARTURIResource ALT_REF;
        public static ARTURIResource PREF_REF;
        public static ARTURIResource HIDDEN_REF;
        public static ARTURIResource IS_REFERENCE_OF;

        public static ARTURIResource LANGUAGE;
        public static ARTURIResource ENTRY;

        static {
            try {
                initialize(VocabUtilities.nodeFactory);
            } catch (VocabularyInitializationException e) {
                e.printStackTrace(System.err);
            }
        }

        public static void initialize(ARTNodeFactory fact) throws VocabularyInitializationException {

            URI = fact.createURIResource(Lemon.URI);
            NAMESPACE = fact.createURIResource(Lemon.NAMESPACE);

            LEXICAL_ENTRY = fact.createURIResource(Lemon.LEXICAL_ENTRY);
            FORM = fact.createURIResource(Lemon.FORM);
            LEXICAL_SENSE = fact.createURIResource(Lemon.LEXICAL_SENSE);
            PART = fact.createURIResource(Lemon.PART);
            PHRASE = fact.createURIResource(Lemon.PHRASE);
            WORD = fact.createURIResource(Lemon.WORD);
            LEXICON = fact.createURIResource(Lemon.LEXICON);

            REPRESENTATION = fact.createURIResource(Lemon.REPRESENTATION);
            WRITTEN_REP = fact.createURIResource(Lemon.WRITTEN_REP);

            LEXICAL_FORM = fact.createURIResource(Lemon.LEXICAL_FORM);
            CANONICAL_FORM = fact.createURIResource(Lemon.CANONICAL_FORM);
            OTHER_FORM = fact.createURIResource(Lemon.OTHER_FORM);
            ABSTRACT_FORM = fact.createURIResource(Lemon.ABSTRACT_FORM);

            SENSE = fact.createURIResource(Lemon.SENSE);
            IS_SENSE_OF = fact.createURIResource(Lemon.IS_SENSE_OF);

            REFERENCE = fact.createURIResource(Lemon.REFERENCE);
            ALT_REF = fact.createURIResource(Lemon.ALT_REF);
            PREF_REF = fact.createURIResource(Lemon.PREF_REF);
            HIDDEN_REF = fact.createURIResource(Lemon.HIDDEN_REF);
            IS_REFERENCE_OF = fact.createURIResource(Lemon.IS_REFERENCE_OF);

            LANGUAGE = fact.createURIResource(Lemon.LANGUAGE);
            ENTRY = fact.createURIResource(Lemon.ENTRY);

        }

    }

}
