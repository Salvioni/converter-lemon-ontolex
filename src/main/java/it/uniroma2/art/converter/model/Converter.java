/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.converter.model;

import it.uniroma2.art.converter.vocabolary.Lemon;
import it.uniroma2.art.ontolex.facilities.OWLArtModelFactoryWithOntoLexModel;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.model.impl.OntoLexModelImpl;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.models.impl.SPARQLBasedRDFTripleModelImpl;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.vocabulary.RDF;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Andrea
 */
public class Converter {

    SPARQLBasedRDFTripleModelImpl model;
    OntoLexModel ontoLexModel;

    public Converter(String sparqlEndPoint) throws ModelCreationException {

        ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();

        OWLArtModelFactoryWithOntoLexModel<? extends ModelConfiguration> fact;
        fact = OWLArtModelFactoryWithOntoLexModel.createModelFactory(factImpl);

        model = new SPARQLBasedRDFTripleModelImpl(fact.loadTripleQueryHTTPConnection(sparqlEndPoint));

        ontoLexModel = new OntoLexModelImpl(fact.createLightweightRDFModel());

    }

    public void convert() {

        convertClasses();
        convertProperties();

    }

    public void write(String path, RDFFormat format) throws IOException, ModelAccessException, UnsupportedRDFFormatException {

        ontoLexModel.writeRDF(new File(path), format);

    }

    private void convertClasses() {

        try (ARTResourceIterator it = model.listSubjectsOfPredObjPair(RDF.Res.TYPE, Lemon.Res.LEXICAL_ENTRY, true)) {

            while (it.hasNext()) {
                ARTResource next = it.next();
                if (next.isURIResource()) {
                    ontoLexModel.addLexicalEntry(next.asURIResource());
                }
            }

        } catch (ModelAccessException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (ARTResourceIterator it = model.listSubjectsOfPredObjPair(RDF.Res.TYPE, Lemon.Res.LEXICAL_SENSE, true)) {

            while (it.hasNext()) {
                ARTResource next = it.next();
                if (next.isURIResource()) {
                    ontoLexModel.addLexicalSense(next.asURIResource());
                }
            }

        } catch (ModelAccessException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (ARTResourceIterator it = model.listSubjectsOfPredObjPair(RDF.Res.TYPE, Lemon.Res.FORM, true)) {

            while (it.hasNext()) {
                ARTResource next = it.next();
                if (next.isURIResource()) {
                    ontoLexModel.addForm(next.asURIResource());
                }
            }

        } catch (ModelAccessException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (ARTResourceIterator it = model.listSubjectsOfPredObjPair(RDF.Res.TYPE, Lemon.Res.PART, true)) {

            while (it.hasNext()) {
                ARTResource next = it.next();
                if (next.isURIResource()) {
                    ontoLexModel.addAffix(next.asURIResource());
                }
            }

        } catch (ModelAccessException | ModelUpdateException ex) {
            ex.printStackTrace();
        }
        try (ARTResourceIterator it = model.listSubjectsOfPredObjPair(RDF.Res.TYPE, Lemon.Res.PHRASE, true)) {

            while (it.hasNext()) {
                ARTResource next = it.next();
                if (next.isURIResource()) {
                    ontoLexModel.addMultiwordExpressions(next.asURIResource());
                }
            }

        } catch (ModelAccessException | ModelUpdateException ex) {
            ex.printStackTrace();
        }
        try (ARTResourceIterator it = model.listSubjectsOfPredObjPair(RDF.Res.TYPE, Lemon.Res.WORD, true)) {

            while (it.hasNext()) {
                ARTResource next = it.next();
                if (next.isURIResource()) {
                    ontoLexModel.addWord(next.asURIResource());
                }
            }

        } catch (ModelAccessException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

    }

    private void convertProperties() {

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.REPRESENTATION)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.addRepresentation(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asLiteral());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.WRITTEN_REP)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.addWrittenRep(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asLiteral());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.LEXICAL_FORM)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.addLexicalForm(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asURIResource());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.CANONICAL_FORM)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.setCanonicalForm(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asURIResource());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }
        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.OTHER_FORM)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.addOtherForm(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asURIResource());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.SENSE)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.addSense(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asURIResource());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.IS_SENSE_OF)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.setIsSenseOf(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asURIResource());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.REFERENCE)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.setReference(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asURIResource());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }

        try (TupleBindingsIterator it = getSubObjOfPedicate(Lemon.Res.IS_REFERENCE_OF)) {

            while (it.hasNext()) {
                TupleBindings next = it.next();

                ontoLexModel.setReference(next.getBoundValue("subject").asURIResource(), next.getBoundValue("object").asURIResource());

            }

        } catch (QueryEvaluationException | UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException | ModelUpdateException ex) {
            ex.printStackTrace();
        }
    }

    private TupleBindingsIterator getSubObjOfPedicate(ARTURIResource predicate) throws QueryEvaluationException, UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {

        TupleQuery query = model.createTupleQuery(QueryLanguage.SPARQL, "select ?subject ?object where{ ?subject " + RDFNodeSerializer.toNT(predicate) + " ?object}", null);
        return query.evaluate(true);

    }

}
