package it.uniroma2.art.converter.test;

import it.uniroma2.art.converter.model.Converter;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import java.io.IOException;

public class Test {

    public static void main(String[] args) {

        try {

            Converter c = new Converter("http://localhost:8080/graphdb-workbench-free/repositories/wordnet");
            c.convert();
            c.write("C:\\Users\\Andrea\\Desktop\\out.n3", RDFFormat.N3);

        } catch (ModelCreationException | IOException | ModelAccessException | UnsupportedRDFFormatException ex) {

        }

    }

}
